package com.javagda24.restapi.controller;

import com.javagda24.restapi.model.dto.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<ErrorMessage> catchException(EntityNotFoundException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorMessage(400, "Entity not found: " + e.getMessage()));
    }
}
