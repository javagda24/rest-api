package com.javagda24.restapi.controller;

import com.javagda24.restapi.model.Grade;
import com.javagda24.restapi.model.dto.AssignGradeToStudentDto;
import com.javagda24.restapi.service.GradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Api(value = "grades", description = "All CRUD operations for grades.")
@RequestMapping(path = "/grade")
public class GradeController {

    @Autowired
    private GradeService gradeService;

    @PostMapping("/assign")
    @ResponseStatus(HttpStatus.OK)
    public void assign(@RequestBody AssignGradeToStudentDto dto){
        gradeService.assign(dto);
    }

    @ApiOperation(value = "Use this endpoint to retrieve all grades from database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Get all grades from database with success."),
            @ApiResponse(code = 403, message = "You do not have permission to get grades from database."),
    })
    @GetMapping("/list")
    public List<Grade> getAll() {
        return gradeService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Grade> get(@PathVariable("id") Long id) {
        Optional<Grade> gradeOptional = gradeService.get(id);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.ok(gradeOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PutMapping("")
    public Long get(@RequestBody Grade grade) {
        return gradeService.insert(grade);
    }

    @PostMapping("")
    public ResponseEntity<Grade> update(@RequestBody Grade grade) {
        Optional<Grade> gradeOptional = gradeService.update(grade);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.ok(gradeOptional.get());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable("id") Long id) {
        gradeService.delete(id);
    }

}
