package com.javagda24.restapi.controller;

import com.javagda24.restapi.model.Student;
import com.javagda24.restapi.model.dto.AddGradeRequestDto;
import com.javagda24.restapi.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.HttpURLConnection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/student")
public class StudentController {

    @Autowired
    private StudentService studentService;


    @PostMapping("/grade")
    public ResponseEntity<Long> addGrade(@RequestBody AddGradeRequestDto dto) {
        Optional<Long> gradeId = studentService.addGrade(dto);
        if (gradeId.isPresent()) {
            return ResponseEntity.ok(gradeId.get());
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    // 1 stwórz GET Mapping na adres /get/{id} - powinien zwracać i wyświetlać studenta z bazy danych
    @GetMapping("/list")
    public List<Student> getAll() {
        return studentService.getAll();
    }

    // 1 stwórz GET Mapping na adres /get/{id} - powinien zwracać i wyświetlać studenta z bazy danych
    @GetMapping("/{id}")
    public ResponseEntity<Student> getByPathVariable(@PathVariable("id") Long studentId) {
        Optional<Student> studentOptional = studentService.getById(studentId);
        if (studentOptional.isPresent()) {
            return ResponseEntity.ok(studentOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    // 2 stwórz GET Mapping na adres /get?studentId=1 - powinien zwracać i wyświetlać studenta z bazy danych
    @GetMapping("")
    public ResponseEntity<Student> getByRequestParam(@RequestParam(value = "studentId", required = true) Long studentId) {
        Optional<Student> studentOptional = studentService.getById(studentId);
        if (studentOptional.isPresent()) {
            return ResponseEntity.ok(studentOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    // C   R   U    D
    // PUT GET POST DELETE
    // POST

    // localhost:8080/student
    @PutMapping("")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void insertStudent(@RequestBody Student student) {
        studentService.insertIntoDatabase(student);
    }

    @PostMapping("")
    public ResponseEntity<Student> edit(@RequestBody Student student) {
        Student studentResult = studentService.update(student);

        return ResponseEntity.ok(studentResult);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable("id") Long studentId) {
        studentService.delete(studentId);
    }
}
