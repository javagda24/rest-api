package com.javagda24.restapi.model.dto;

import com.javagda24.restapi.model.GradeSubject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddGradeRequestDto {
    private Long studentId;
    private double gradeValue;
    private GradeSubject subject;
}
