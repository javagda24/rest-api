package com.javagda24.restapi.service;

import com.javagda24.restapi.model.Grade;
import com.javagda24.restapi.model.Student;
import com.javagda24.restapi.model.dto.AssignGradeToStudentDto;
import com.javagda24.restapi.repository.GradeRepository;
import com.javagda24.restapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class GradeService {
    @Autowired
    private GradeRepository gradeRepository;
    @Autowired
    private StudentRepository studentRepository;

    public List<Grade> getAll() {
        return gradeRepository.findAll();
    }

    public Optional<Grade> get(Long id) {
        return gradeRepository.findById(id);
    }

    public Long insert(Grade grade) {
        return gradeRepository.save(grade).getId();
    }

    public Optional<Grade> update(Grade grade) {
        if (grade.getId() != null) {
            return Optional.of(gradeRepository.save(grade));
        }
        return Optional.empty();
    }

    public void delete(Long id) {
        if(gradeRepository.existsById(id)){
            gradeRepository.deleteById(id);
        }
    }

    public void assign(AssignGradeToStudentDto dto) {
        if(!studentRepository.existsById(dto.getStudentId())){
            throw new EntityNotFoundException("Student");
        }
        if(!gradeRepository.existsById(dto.getGradeId())){
            throw new EntityNotFoundException("Grade");
        }
        Student student = studentRepository.getOne(dto.getStudentId());
        Optional<Grade> gradeOpt = gradeRepository.findById(dto.getGradeId());
        Grade grade = gradeOpt.get();

        grade.setStudent(student);
        gradeRepository.save(grade);
    }
}
