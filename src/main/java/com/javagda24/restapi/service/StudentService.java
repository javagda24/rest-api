package com.javagda24.restapi.service;

import com.javagda24.restapi.model.Grade;
import com.javagda24.restapi.model.Student;
import com.javagda24.restapi.model.dto.AddGradeRequestDto;
import com.javagda24.restapi.repository.GradeRepository;
import com.javagda24.restapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private GradeRepository gradeRepository;

    public Optional<Student> getById(Long studentId) {
        return studentRepository.findById(studentId);
    }

    public void insertIntoDatabase(Student student) {
        // jeśli chcesz mieć pewność że rekord będzie dodany, a nie zostanie aktualizowany to wyzeruj mu id:
        student.setId(null);
        studentRepository.save(student);
    }

    public Student update(Student student) {
        if (student.getId() != null) {
            return studentRepository.save(student);
        }
        throw new EntityNotFoundException("Id not provided.");
    }

    public void delete(Long studentId) {
        if (studentRepository.existsById(studentId)) {
            studentRepository.deleteById(studentId);
        }
    }

    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    public Optional<Long> addGrade(AddGradeRequestDto dto) {
        Optional<Student> studentOptional = studentRepository.findById(dto.getStudentId());
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();

            Grade grade = new Grade(dto.getSubject(), dto.getGradeValue());
            grade.setStudent(student);
            return Optional.of(gradeRepository.save(grade).getId());
        }
        return Optional.empty();
    }
}
