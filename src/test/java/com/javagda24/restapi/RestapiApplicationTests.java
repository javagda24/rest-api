package com.javagda24.restapi;

import com.javagda24.restapi.controller.StudentController;
import com.javagda24.restapi.model.Student;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestapiApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RestapiApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {
        Student student = new Student();
        student.setFirstName("pawel");
        student.setLastName("gawel");

        restTemplate.put("/student", student);

        ResponseEntity studentList = restTemplate.getForEntity("/student/list", ResponseEntity.class);
//        Assert.assertTrue(studentList.contains(student));
        System.out.println(studentList);
    }

}
